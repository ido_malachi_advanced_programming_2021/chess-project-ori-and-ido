Instructions:

1. Download all the files from the repository.
2. Open the project in VS (by .sln) or by any other IDE.
3. Compile the code by c\c++ compiler.
4. Run the GUI frontend by click on chessGraphic.exe
5. When the chess board is up: run chessGame.cpp and start play:)
6. Enjoy!

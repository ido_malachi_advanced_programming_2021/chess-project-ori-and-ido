#include "Bishop.h"

/*
The function initializes a new Bishop object
*/
Bishop::Bishop(const int color, const int position[SIZE_OF_POS]):
	Piece(color, position)
{
	setType(BISHOP);
}


/*
The function returns true if the movement from the source to the destenation is valid for a Bishop object
Input: the source and destenation
Output: True if valid, else false
*/
bool Bishop::checkMove(const int dest[SIZE_OF_POS])
{
	int xIndexDifference =  dest[X_INDEX] - this->getPosition()[X_INDEX];
	int yIndexDifference = dest[Y_INDEX] - this->getPosition()[Y_INDEX];
	if (abs(xIndexDifference) == abs(yIndexDifference)) //noticed that in every valid move at an angle the absolut value of the difference between the source x index
		//and the destenation x index is alaways equal to the absolut value of the difference between the source y index and the destenation y index
	{
		return true; //movement is valid
	}
	return false; //invalid movement for a Bishop
}
  
/*
The function checks if the path until the destenation is blocked by other pieces and returns true if so
Input: pointer to the source square, pointer to the destenation square, pointer to the board that contains all the squares
Output: true if the path is blocked, else false
*/
bool Bishop::isBlocked(int* dest, Piece* board[][CHESS_BOARD_LEN])
{
	int xIndexDifference = dest[X_INDEX] - this->getPosition()[X_INDEX];
	int yIndexDifference = dest[Y_INDEX] - this->getPosition()[Y_INDEX];
	int currX = this->getPosition()[X_INDEX];
	int currY = this->getPosition()[Y_INDEX];

	if (xIndexDifference < 0 && xIndexDifference == yIndexDifference) //going up and left
	{
		currX--;
		currY--;
		while (currX > dest[X_INDEX]) //going over all the squares in the bishop's way to its destenation
		{
			if (board[currX][currY] != nullptr) //if the current square contains a piece will return true
			{
				return true;
			}
			currX--;
			currY--;
		}
	}
	else if (xIndexDifference > 0 && xIndexDifference == yIndexDifference) //going down and right
	{
		currX++;
		currY++;
		while (currX < dest[X_INDEX]) //going over all the squares in the bishop's way to its destenation
		{
			if (board[currX][currY] != nullptr) //if the current square contains a piece will return true
			{
				return true;
			}
			currX++;
			currY++;
		}
	}
	else if (xIndexDifference < 0 && yIndexDifference > 0) //going down and left
	{
		currX--;
		currY++;
		while (currX > dest[X_INDEX]) //going over all the squares in the bishop's way to its destenation
		{
			if (board[currX][currY] != nullptr) //if the current square contains a piece will return true
			{
				return true;
			}
			currX--;
			currY++;
		}
	}
	else //going up and right
	{
		currX++;
		currY--;
		while (currX < dest[X_INDEX]) //going over all the squares in the bishop's way to its destenation
		{
			if (board[currX][currY] != nullptr) //if the current square contains a piece will return true
			{
				return true;
			}
			currX++;
			currY--;
		}
	}
	return false;
}
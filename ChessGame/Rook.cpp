#include "Rook.h"

/*
The function initializes a new Rook object
*/
Rook::Rook(const int color, const int position[SIZE_OF_POS]) :
	Piece(color, position)
{
	setType(ROOK);
}

  
/*
The function returns true if the movement from the source to the destenation is valid for a rook object
Input: the source and destenation 
Output: True if valid, else false
*/
bool Rook::checkMove(const int dest[SIZE_OF_POS])
{
	if (this->getPosition()[Y_INDEX] != dest[Y_INDEX] && this->getPosition()[X_INDEX] != dest[X_INDEX])//cheking the validity of the x and y index values
	{
		return false; //x or y index values aren't valid
	}
	return true; //if the x and y values are both valid will return true
}


/*
The function checks if the path until the destenation is blocked by other pieces and returns true if so
Input: pointer to the source square, pointer to the destenation square, pointer to the board that contains all the squares
Output: true if the apth is blocked, else false
*/
bool Rook::isBlocked(int* dest, Piece* board[][CHESS_BOARD_LEN])
{
	int max = 0;
	int min = 0;
	int constValue = 0;
	if (this->getPosition()[X_INDEX] == dest[X_INDEX]) //if moving verticaly will check the relevant colomn
	{
		constValue = this->getPosition()[X_INDEX];
		max = std::max(this->getPosition()[Y_INDEX], dest[Y_INDEX]); //getting the bigger y value - destenation or the source
		min = std::min(this->getPosition()[Y_INDEX], dest[Y_INDEX]); //getting the samller y value - destenation or the source
		min++; //Don't need to check the source piece

		while (min < max) //going over all the squares in the rook's way to its destenation
		{
			if (board[constValue][min] != nullptr) //if the current square contains a piece will return true
			{
				return true;
			}
			min++;
		}
	}
	else //if moving horizontaly will check the relevant raw
	{
		constValue = this->getPosition()[Y_INDEX];
		max = std::max(this->getPosition()[X_INDEX], dest[X_INDEX]); //getting the biggest x value - destenation or the source
		min = std::min(this->getPosition()[X_INDEX], dest[X_INDEX]); //getting the samller x value - destenation or the source
		min++;//Don't need to check the source piece

		while (min < max) //going over all the squares in the rook's way to its destenation
		{
			if (board[min][constValue] != nullptr) //if the current square contains a piece will return true 
			{
				return true;
			}
			min++;
		}
	}
	return false;
}
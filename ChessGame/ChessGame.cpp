﻿// ChessGame.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
/*
#include <iostream>
#include "FrontHandler.h"
   
int main()
{
    char* message = new char[MESSAGE_LEN];
    int src[SIZE_OF_POS] = { 0 };
    int dest[SIZE_OF_POS] = { 0 };
    Piece* saveDest;
    Menu::printWelcomeMessage();
    FrontHandler::sendStartMessage();
    Menu* chessMenu = new Menu();
    chessMenu->createDefaultBoard();
    FrontHandler* myHandler = new FrontHandler(chessMenu);

    //game duration
    while (!chessMenu->isChess(Menu::getTurn()))
    {
        try
        {
            chessMenu->printBoard();

            //getting input from user and spliting it to source and destenation
            message = myHandler->getMessage();
            FrontHandler::splitSrcAndDest(message, src, dest);

            //checking move validity
            chessMenu->indexIsValid(src, dest);
            chessMenu->sourceIsValid(src);
            chessMenu->destIsValid(dest);
            chessMenu->checkPieceMove(src, dest);
            saveDest = &chessMenu->getBoard()[dest[X_INDEX]][Y_INDEX];
            chessMenu->movePiece(src, dest);
            if (chessMenu->isChess(!Menu::getTurn()))
            {
                chessMenu->movePiece(dest, src);
                chessMenu->getBoard()[dest[X_INDEX]][Y_INDEX] = *saveDest;
            }

            myHandler->sendMessage(message);

            Menu::changeTurn();
        }
        catch (const char* e)
        {
            std::cout << e << std::endl;
            myHandler->sendMessage(e);
        }
    }

    delete[] message;
    delete myHandler;
    chessMenu = nullptr;


    return 0;
}
*/
// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file

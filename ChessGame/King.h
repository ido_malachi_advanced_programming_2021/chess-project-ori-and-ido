#pragma once
#include "Piece.h"
#include "Menu.h"
class King : public Piece
{
public:

	King(const int color, const int position[SIZE_OF_POS]);
	bool checkMove(const int dest[SIZE_OF_POS]) override;
	bool virtual isBlocked(int* dest, Piece* board[][CHESS_BOARD_LEN]) override;
	bool isThreatened(Piece* board[][CHESS_BOARD_LEN]) override;
};

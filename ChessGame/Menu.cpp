#include "Menu.h"

bool Menu::_whitesTurn = true;

void Menu::changeTurn()
{
	_whitesTurn = !_whitesTurn;
}

bool Menu::getTurn()
{
	return _whitesTurn;
}


/*
The function initializes a new Menu object
Input: the object's color (team), and its position on the board
Output: None
*/
Menu::Menu():
	_board()
{}  


/*
The function delets a Menu object
Input: None
Output: None
*/
Menu::~Menu()
{
	for (int i = 0; i < CHESS_BOARD_LEN; i++)
	{
		for (int j = 0; j < CHESS_BOARD_LEN; j++)
		{
			delete this->_board[i][j];
		}
	}
}


/*
The function prints welcome message to the user (for console)
*/
void Menu::printWelcomeMessage()
{
	std::cout << "Welcome to OrIdo's chess game! \nMay the best team win!" << std::endl;
}


/*
The function creates the starting board of the game chess
Input: None
Output: Pointer to the new default board created
*/
Piece** Menu::createDefaultBoard()
{
	int color = 0;
	int* pos = new int[SIZE_OF_POS];
	for (int i = 0; i < CHESS_BOARD_LEN; i++)
	{
		if (i > 2)
		{
			color = 1;
		}
		for (int j = 0; j < CHESS_BOARD_LEN; j++)
		{
			if ((i == 0 || i == 7)  &&  (j == 0 || j == 7))
			{
				pos[X_INDEX] = i, pos[Y_INDEX] = j;
				_board[i][j] = new Rook(color, pos);
			}
			else if ((i == 0 && j == 3) || (i == 7 && j == 3))
			{
				pos[X_INDEX] = i, pos[Y_INDEX] = j;
				_board[i][j] = new King(color, pos);
			}
			else if ((i == 0 && j == 4) || (i == 7 && j == 4))
			{
				pos[X_INDEX] = i, pos[Y_INDEX] = j;
				_board[i][j] = new Queen(color, pos);
			}
			else if ((i == 0 || i == 7)  &&  (j == 2 || j == 5))
			{
				pos[X_INDEX] = i, pos[Y_INDEX] = j;
				_board[i][j] = new Bishop(color, pos);
			}
			else if ((i == 0 || i ==7) && (j == 1 || j == 6))
			{
				pos[X_INDEX] = i, pos[Y_INDEX] = j;
				_board[i][j] = new Knight(color, pos);
			}
			else if (i == 1 || i == 6)
			{
				pos[X_INDEX] = i, pos[Y_INDEX] = j;
				_board[i][j] = new Pawn(color, pos);
			}
			else
			{
				_board[i][j] = nullptr;
			}
		}
	}
	return *_board;
}


/*
The function prints the field _board
Input: None
Output:None
*/
void Menu::printBoard()
{
	char pieceLetter = ' ';
	for (int i = 0; i < CHESS_BOARD_LEN; i++)
	{
		for (int j = 0; j < CHESS_BOARD_LEN; j++)
		{
			if (_board[i][j])
			{
				switch (_board[i][j]->getType())
				{
				case KING:
					pieceLetter = 'k';
					break;
				case ROOK:
					pieceLetter = 'r';
					break;
				case BISHOP:
					pieceLetter = 'b';
					break;
				case PAWN:
					pieceLetter = 'p';
					break;
				case QUEEN:
					pieceLetter = 'q';
					break;
				case KNIGHT:
					pieceLetter = 'n';
					break;
				}
				if (_board[i][j]->getColor() == 0) //if the piece is white then its letter should be upper case
				{
					pieceLetter = toupper(pieceLetter);
				}
				std::cout << pieceLetter << " ";
			}
			else
			{
				std::cout << "# ";
			}
		}
		std::cout << std::endl;
	}
}

/*
The function returns the field _board
Input: None
Output: A pointer to the board
*/
Piece** Menu::getBoard()
{
	return *_board;
}


void Menu::indexIsValid(const int src[SIZE_OF_POS], const int dest[SIZE_OF_POS])
{
	char* throwString = new char[SIZE_OF_POS];
	if (src[X_INDEX] == dest[X_INDEX] && src[Y_INDEX] == dest[Y_INDEX]) //if the source index equales to the destenation index will throw an exception
	{
		std::cout << "Invalid move - source square and destenation square indexes are equale!" << std::endl;
		throwString[0] = '7';
		throwString[1] = NULL;
		throw throwString;
	}
}


/*
The function throws an error if the source square is nullptr or contains a piece from the other player's team
Input: source square to check
Output: None
*/
void Menu::sourceIsValid(const int* src)
{
	char* throwString = new char[SIZE_OF_POS];
	int x = src[X_INDEX];
	int y = src[Y_INDEX];
	Piece *sourcePiece = _board[x][y];
	if (nullptr == sourcePiece) //if source is empty or contains a piece from the rival's team will throw exception
	{
		sourcePiece = nullptr;
		std::cout << "Invalid move - there isn't a soldier of the current player in the source square" << std::endl;
		throwString[X_INDEX] = '2';
		throwString[Y_INDEX] = NULL;
		throw throwString;
	}
	else if ((!sourcePiece->getColor() && !_whitesTurn) || (sourcePiece->getColor() && _whitesTurn))
	{
		sourcePiece = nullptr;
		std::cout << "Invalid move - there isn't a soldier of the current player in the source square" << std::endl;
		throwString[X_INDEX] = '2';
		throwString[Y_INDEX] = NULL;
		throw throwString;
	}
	sourcePiece = nullptr;
}


/*
The function throws an error if the destenation square contains a piece from the player's team
Input: destenation square to check
Output: None
*/
void Menu::destIsValid(const int* dest)
{
	char* throwString = new char[SIZE_OF_POS];
	int x = dest[X_INDEX];
	int y = dest[Y_INDEX];
	Piece* destPiece = _board[x][y];
	if (!destPiece)
	{
		destPiece = nullptr;
		return;
	}
	else if ((!destPiece->getColor() && _whitesTurn) || (destPiece->getColor() && !_whitesTurn)) //if dest contains a piece from the same team that plays the current turn will throw exception
	{
		destPiece = nullptr;
		std::cout<< "Invalid move - there is a soldier of the current player in the destenation square"  <<std::endl;
		throwString[X_INDEX] = '3';
		throwString[Y_INDEX] = NULL;
		throw throwString;
	}
	destPiece = nullptr;
}



/*
The function throws an error if the move of the piece in the source to the destenation is invalid or the path is blocked
Input: source and destenation square
Output: None
*/
void Menu::checkPieceMove(const int src[SIZE_OF_POS], int dest[SIZE_OF_POS])
{
	char* throwString = new char[SIZE_OF_POS];
	int srcX = src[X_INDEX];
	int srcY = src[Y_INDEX];
	int destX = dest[X_INDEX];
	int destY = dest[Y_INDEX];
	Piece* srcPiece = _board[srcX][srcY];
	Piece* destPiece = _board[destX][destY];
	if (!srcPiece->checkMove(dest))
	{
		throwString[X_INDEX] = '6';
		throwString[Y_INDEX] = NULL;
		throw throwString;
	}
	else if (srcPiece->isBlocked(dest, this->_board))
	{
		throwString[X_INDEX] = '6';
		throwString[Y_INDEX] = NULL;
		throw throwString;
	}

	srcPiece = nullptr;
	destPiece = nullptr;
}


/*
The function moves a piece from source to destenation
Input: source of piece and its destenation
Output: None
*/
void Menu::movePiece(const int src[SIZE_OF_POS], const int dest[SIZE_OF_POS])
{
	int srcX = src[X_INDEX];
	int srcY = src[Y_INDEX];
	int destX = dest[X_INDEX];
	int destY = dest[Y_INDEX];
	this->_board[srcX][srcY]->setPosition(dest); //updating the source position aftr moving it
	if (this->_board[dest[X_INDEX]][dest[Y_INDEX]]) //if there is a piece in the destenation will delete it
	{
		delete this->_board[destX][destY];
	}
	this->_board[destX][destY] = this->_board[srcX][srcY];
	this->_board[srcX][srcY] = nullptr;
}


/*
The function returns true if the given player's king is threatened, else false
Input: True to check the black king, false to check the white king
Output: True if the given player's king is threatened, else false
*/
bool Menu::isChess(bool kingToCheck)
{
	for (int i = 0; i < CHESS_BOARD_LEN; i++)
	{
		for (int j = 0; j < CHESS_BOARD_LEN; j++)
		{
			if (_board[i][j])
			{
				if (_board[i][j]->getType() == KING && ((kingToCheck && _board[i][j]->getColor()) || (!kingToCheck && !_board[i][j]->getColor()))) //if the current piece is the team's king will check if it is thretened
				{
					return _board[i][j]->isThreatened(this->_board);
				}
			}
		}
	}
}
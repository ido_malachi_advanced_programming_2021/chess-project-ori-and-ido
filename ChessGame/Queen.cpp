#include "Queen.h"

/*
The function initializes a new Queen object
*/
Queen::Queen(const int color, const int position[SIZE_OF_POS]) : Piece(color, position)
{
	this->setType(QUEEN);
}

/*
The function returns true if the movement from the source to the destenation is valid for a Queen object
Input: the source and destenation
Output: True if valid, else false
*/
bool Queen::checkMove(const int dest[SIZE_OF_POS])
{
	//check if the distance from the destination indexes is valid
	//queen can move at an angle as a Bishop and verticly or horizontaly as a Rook, therefore using same check as Bishop and Rook
	int xIndexDifference = dest[X_INDEX] - this->getPosition()[X_INDEX];
	int yIndexDifference = dest[Y_INDEX] - this->getPosition()[Y_INDEX];
	if (abs(xIndexDifference) == abs(yIndexDifference)) //check if moving at an angle (same as Bishop)
	{
		return true; //movement is valid
	}
	else if (this->getPosition()[Y_INDEX] == dest[Y_INDEX] || this->getPosition()[X_INDEX] == dest[X_INDEX])  //check if moving horizontaly or verticaly
	{
		return true; //x or y index values are valid
	}
	return false;
}


/*
The function checks if the path until the destenation is blocked by other pieces and returns true if so
Input: pointer to the source square, pointer to the destenation square, pointer to the board that contains all the squares
Output: true if the path is blocked, else false
*/
bool Queen::isBlocked(int* dest, Piece* board[][CHESS_BOARD_LEN])
{
	int xIndexDifference = dest[X_INDEX] - this->getPosition()[X_INDEX];
	int yIndexDifference = dest[Y_INDEX] - this->getPosition()[Y_INDEX];
	int currX = this->getPosition()[X_INDEX];
	int currY = this->getPosition()[Y_INDEX];

	if (dest[X_INDEX] != this->getPosition()[X_INDEX]  &&  dest[Y_INDEX] != this->getPosition()[Y_INDEX])//If moving in an angle checking angle move, else cheking horizontal/ vertical movement
	{
		if (xIndexDifference < 0 && xIndexDifference == yIndexDifference) //going up and left
		{
			currX--;
			currY--;
			while (currX > dest[X_INDEX]) //going over all the squares in the queen's way to its destenation
			{
				if (board[currX][currY] != nullptr) //if the current square contains a piece will return true
				{
					return true;
				}
				currX--;
				currY--;
			}
		}
		else if (xIndexDifference > 0 && xIndexDifference == yIndexDifference) //going down and right
		{
			currX++;
			currY++;
			while (currX < dest[X_INDEX]) //going over all the squares in the queen's way to its destenation
			{
				if (board[currX][currY] != nullptr) //if the current square contains a piece will return true
				{
					return true;
				}
				currX++;
				currY++;
			}
		}
		else if (xIndexDifference < 0 && yIndexDifference > 0) //going down and left
		{
			currX--;
			currY++;
			while (currX > dest[X_INDEX]) //going over all the squares in the queen's way to its destenation
			{
				if (board[currX][currY] != nullptr) //if the current square contains a piece will return true
				{
					return true;
				}
				currX--;
				currY++;
			}
		}
		else //going up and right
		{
			currX++;
			currY--;
			while (currX < dest[X_INDEX]) //going over all the squares in the queen's way to its destenation
			{
				if (board[currX][currY] != nullptr) //if the current square contains a piece will return true
				{
					return true;
				}
				currX++;
				currY--;
			}
		}
		return false;
	}


	//If moving horizontaly or vertecaly will skip the previous condition and run the following check 
	int max = 0;
	int min = 0;
	int constValue = 0;
	if (this->getPosition()[X_INDEX] == dest[X_INDEX]) //if moving verticaly will check the relevant colomn
	{
		constValue = this->getPosition()[X_INDEX];
		max = std::max(this->getPosition()[Y_INDEX], dest[Y_INDEX]); //getting the bigger y value - destenation or the source
		min = std::min(this->getPosition()[Y_INDEX], dest[Y_INDEX]); //getting the samller y value - destenation or the source
		min++; //Don't need to check the source piece

		while (min < max) //going over all the squares in the rook's way to its destenation
		{
			if (board[constValue][min] != nullptr) //if the current square contains a piece will return true
			{
				return true;
			}
			min++;
		}
	}
	else //if moving horizontaly will check the relevant raw
	{
		constValue = this->getPosition()[Y_INDEX];
		max = std::max(this->getPosition()[X_INDEX], dest[X_INDEX]); //getting the biggest x value - destenation or the source
		min = std::min(this->getPosition()[X_INDEX], dest[X_INDEX]); //getting the samller x value - destenation or the source
		min++;//Don't need to check the source piece

		while (min < max) //going over all the squares in the queen's way to its destenation
		{
			if (board[min][constValue] != nullptr) //if the current square contains a piece will return true 
			{
				return true;
			}
			min++;
		}
	}
	return false;
}

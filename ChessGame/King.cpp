#include "King.h"

/*
The function initializes a new king object
Input: the object's color (team), and its position on the board
Output: None
*/
King::King(const int color, const int position[SIZE_OF_POS]) :
	Piece(color, position)
{
	setType(KING);
}
  
/*
The function returns true if the movement from the source to the destenation is valid for a king object
Input: the source and destenation
Output: True if valid, else false
*/
bool King::checkMove(const int dest[SIZE_OF_POS])
{
	if (this->getPosition()[Y_INDEX] + 1 < dest[Y_INDEX] || this->getPosition()[Y_INDEX] - 1 > dest[Y_INDEX])//cheking the validity of the y index values
	{
		return false; //y index values aren't valid
	}
	else if (this->getPosition()[X_INDEX] + 1 < dest[X_INDEX] || this->getPosition()[X_INDEX] - 1 > dest[X_INDEX])//cheking the validity of the x index values
	{
		return false; //x index values aren't valid
	}
	return true; //if the x and y values are both valid will return true
}


/*
The function checks if the path until the destenation is blocked by other pieces and returns true if so
Input: pointer to the source square, pointer to the destenation square, pointer to the board that contains all the squares
Output: true if the apth is blocked, else false
*/
bool King::isBlocked(int* dest, Piece* board[][CHESS_BOARD_LEN])
{
	if (!board[dest[X_INDEX]][dest[Y_INDEX]] || board[dest[X_INDEX]][dest[Y_INDEX]]->getColor() != this->getColor())//if the destenation is nullptr or in a different color than source - isn't blocked
	{
		return false;
	}
	return true; //if the destenation contains a piece from the same team as the source piece - is blocked!
}


/*
The function cheks if the king is threatened by one of the other team's pieces
Input: Board to check other team's pieces on
Output: True if threatened, else false
*/
bool King::isThreatened(Piece *board[][CHESS_BOARD_LEN])
{
	//Menu::printAllBoard(board);
	for (int i = 0; i < CHESS_BOARD_LEN; i++)
	{
		for (int j = 0; j < CHESS_BOARD_LEN; j++)
		{
			if (board[i][j] != nullptr)
			{
				if ((this->getColor() && !board[i][j]->getColor()) || (!this->getColor() && board[i][j]->getColor())) //if the current piece is the team's king will check if it is thretened
				{
					if (!board[i][j]->isBlocked(this->getPosition(), board) && board[i][j]->checkMove(this->getPosition())) //if the piece path to the king isn't blocked and it can eat the king in a move returns true
					{
						return true;
					}
				}
			}
		}
	}
	return false; //chekced the whole board - the king is secured
} 
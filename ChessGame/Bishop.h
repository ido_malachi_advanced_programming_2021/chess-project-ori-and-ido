#pragma once
#include "Piece.h"

class Bishop : public Piece
{
public:
	Bishop(const int color, const int position[SIZE_OF_POS]);
	bool checkMove(const int dest[SIZE_OF_POS]) override;
	bool isBlocked(int* dest, Piece* board[][CHESS_BOARD_LEN]) override;
};

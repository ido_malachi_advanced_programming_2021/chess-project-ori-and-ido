#pragma once
#include <iostream>
#include <String>
#include "Type.h"

#define CHESS_BOARD_LEN 8
#define SIZE_OF_POS 2
#define X_INDEX 0
#define Y_INDEX 1

class Piece
{
private:
	int _color;
	int _position[SIZE_OF_POS];
	Type _type;

public:
	//C'tor , D'tor
	Piece(const int color, const int position[SIZE_OF_POS]);

	//Getters
	int getColor() const;
	int* getPosition();
	Type getType() const;
	   
	//Setters
	void setPosition(const int position[SIZE_OF_POS]);
	void setType(const Type type);
	
	bool virtual isThreatened(Piece* board[][CHESS_BOARD_LEN]);
	//Pure piece methods

	bool virtual checkMove(const int dest[SIZE_OF_POS]) = 0;
	bool virtual isBlocked(int* dest, Piece* board[][CHESS_BOARD_LEN]) = 0;
	
};
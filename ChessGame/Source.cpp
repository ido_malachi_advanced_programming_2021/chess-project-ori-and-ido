/*
This file servers as an example of how to use Pipe.h file.
It is recommended to use the following code in your project, 
in order to read and write information from and to the Backend
*/

#include "FrontHandler.h"
#include "Pipe.h"
#include <iostream>
#include <thread>

using std::cout;
using std::endl;
using std::string;


void main()
{
	srand(time_t(NULL));

	
	Pipe p;
	bool isConnect = p.connect();
	
	string ans;
	while (!isConnect)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		std::cin >> ans;

		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = p.connect();
		}
		else 
		{
			p.close();
			return;
		}
	}
	

	char msgToGraphics[1024];
	// msgToGraphics should contain the board string accord the protocol
	// YOUR CODE

	strcpy_s(msgToGraphics, "RNBKQBNRPPPPPPPP################################pppppppprnbkqbnr0");
	
	p.sendMessageToGraphics(msgToGraphics);   // send the board string

	// get message from graphics
	string msgFromGraphics = p.getMessageFromGraphics();
	int src[SIZE_OF_POS] = { 0 };
	int dest[SIZE_OF_POS] = { 0 };
	Piece* saveDest;
	Menu* chessMenu = new Menu();
	chessMenu->createDefaultBoard();
	FrontHandler* myHandler = new FrontHandler(chessMenu);

	while (msgFromGraphics != "quit")
	{
		try
		{

			FrontHandler::splitSrcAndDest(msgFromGraphics, src, dest);

			//checking move validity
			chessMenu->indexIsValid(src, dest);
			chessMenu->sourceIsValid(src);
			chessMenu->destIsValid(dest);
			chessMenu->checkPieceMove(src, dest);
			saveDest = &chessMenu->getBoard()[dest[X_INDEX]][dest[Y_INDEX]]; //saving destenation value in case prefoeming check on itsself and will need to regain the piece
			chessMenu->movePiece(src, dest);
			if (chessMenu->isChess(!Menu::getTurn())) //if check on the player itself will return the board to its order before the move
			{
				chessMenu->movePiece(dest, src);  //returning the piece that was moved to its source place
				chessMenu->getBoard()[dest[X_INDEX]][dest[Y_INDEX]] = *saveDest;  //returning the eaten piece to the destenation position (NULL if dest didn't contain a piece)
				strcpy_s(msgToGraphics, "4");
			}
			else if (chessMenu->isChess(Menu::getTurn()))//if check on the rival will send 1 to the front
			{
				strcpy_s(msgToGraphics, "1");
			}
			else
			{
				strcpy_s(msgToGraphics, "0"); //valid move - no exception was thrown and check wasn't commited on both sides
			}

			if (msgToGraphics[X_INDEX] != '4') //if didn't perform check on himself the turn filps to the other player
			{
				Menu::changeTurn();
			}
		}
		catch (char* e)
		{
			strcpy_s(msgToGraphics, e);//catching exception thrown and inserting it into msgToGraphics
		}

		// YOUR CODE
		 // msgToGraphics should contain the result of the operation

		/******* JUST FOR EREZ DEBUGGING ******/
		//int r = rand() % 10; // just for debugging......
		//msgToGraphics[0] = (char)(1 + '0');
		//msgToGraphics[1] = 0;
		/******* JUST FOR EREZ DEBUGGING ******/


		// return result to graphics		
		p.sendMessageToGraphics(msgToGraphics);   

		// get message from graphics
		msgFromGraphics = p.getMessageFromGraphics();
	}

	p.close();
}
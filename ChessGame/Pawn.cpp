#include "Pawn.h"

/*
The function initializes a new Pawn object
*/
Pawn::Pawn(const int color, const int position[SIZE_OF_POS]) : Piece(color, position)
{
	_startingLine = position[X_INDEX];
	this->setType(PAWN);
}

/*
The function returns true if the movement from the source to the destenation is valid for a Pawn object
Input: the source and destenation
Output: True if valid, else false
*/
bool Pawn::checkMove(const int dest[SIZE_OF_POS])
{
	int xDistance = abs(dest[Y_INDEX] - this->getPosition()[Y_INDEX]);
	int yDistance = abs(dest[X_INDEX] - this->getPosition()[X_INDEX]);
	if ((_startingLine == 1 && dest[X_INDEX] > 1 && dest[X_INDEX] > this->getPosition()[X_INDEX]) || (_startingLine == 6 && dest[X_INDEX] < 6 && dest[X_INDEX] < this->getPosition()[X_INDEX])) //check for Pawns who started at the top
	{
		if (((this->getPosition()[X_INDEX] == 6 && _startingLine == 6) || (this->getPosition()[X_INDEX] == 1 && _startingLine == 1)) && (xDistance == 0 && yDistance == 2))//cheking if hasn't moved yet and moving two steps forward
		{
			return true;
		}
		else if ((xDistance == 0 || xDistance == 1) && yDistance == 1) //checking if moving one step forward
		{
			return true;
		}
	}
	return false;//invalid movement
}


/*
The function checks if the path until the destenation is blocked by other pieces and returns true if so
Input: pointer to the source square, pointer to the destenation square, pointer to the board that contains all the squares
Output: true if the path is blocked, else false
*/
bool Pawn::isBlocked(int* dest, Piece* board[][CHESS_BOARD_LEN])
{
	int xDistance = abs(dest[Y_INDEX] - this->getPosition()[Y_INDEX]);
	int yDistance = abs(dest[X_INDEX] - this->getPosition()[X_INDEX]);
	if (xDistance == 0 && yDistance == 1)
	{
		if (board[dest[X_INDEX]][dest[Y_INDEX]] != nullptr) //if the square in between contains a piece the path is blocked - returns true
		{
			return true;
		}
	}
	else if (xDistance == 0 && yDistance == 2) //if moving two steps forward checks the square in between
	{
		if (board[dest[X_INDEX]][dest[Y_INDEX]] != nullptr) //if the destenation square contains a piece the path is blocked - returns true
		{
			return true;
		}
		else if (_startingLine == 1) //check for pawn that moves up: if the square in between contains a piece the path is blocked - returns true
		{
			if (board[dest[X_INDEX]-1][dest[Y_INDEX]] != nullptr)
			{
				return true;
			}
		}
		else if (_startingLine == 6) // check for pawn that moves down: if the square in between contains a piece the path is blocked - returns true
		{
			if (board[dest[X_INDEX]+1][dest[Y_INDEX]] != nullptr)
			{
				return true;
			}
		}
	}
	else if (xDistance == 1 && yDistance == 1)//checking if moving one step on an angle
	{
		if (board[dest[X_INDEX]][dest[Y_INDEX]] == nullptr || board[dest[X_INDEX]][dest[Y_INDEX]]->getColor() == this->getColor())
		{
			return true;
		}
	}
	return false; //the path isn't blocked returns false
}
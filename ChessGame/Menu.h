#pragma once
#include "Piece.h"
#include "Pawn.h"
#include "King.h"
#include "Queen.h"
#include "Rook.h"
#include "Bishop.h"
#include "Knight.h"

#define SMALLEST_INDEX_NUM 0
#define BIGGEST_INDEX_NUM 7

#define ASCII_VALUE_OF_0 48
#define ASCII_VALUE_OF_A 96

class Menu
{
private:
	static bool _whitesTurn;
	Piece* _board[CHESS_BOARD_LEN][CHESS_BOARD_LEN] = { 0 };

public:
	Menu();
	~Menu();
	static void printWelcomeMessage();
	static void changeTurn();
	static bool getTurn();

	Piece** createDefaultBoard();
	void printBoard();
	Piece** getBoard();

	void indexIsValid(const int src[SIZE_OF_POS], const int dest[SIZE_OF_POS]);
	void sourceIsValid(const int* src);
	void destIsValid(const int* dest);
	void checkPieceMove(const int src[SIZE_OF_POS], int dest[SIZE_OF_POS]);
	bool isChess(bool kingToCheck);

	void movePiece(const int src[SIZE_OF_POS], const int dest[SIZE_OF_POS]);
};

#pragma once
#include "Piece.h"
#define VALID_FIRST_DISTANCE 1
#define VALID_SECOND_DISTANCE 2


class Knight : public Piece
{
public:
	Knight(const int color, const int position[SIZE_OF_POS]);
	bool checkMove(const int dest[SIZE_OF_POS]) override;
	bool isBlocked(int* dest, Piece* board[][CHESS_BOARD_LEN]) override;
};
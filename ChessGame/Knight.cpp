#include "Knight.h"

Knight::Knight(const int color, const int position[SIZE_OF_POS]) : Piece(color, position)
{
	this->setType(KNIGHT);
}

/*
The function returns true if the movement from the source to the destenation is valid for a Kngiht object
Input: the source and destenation
Output: True if valid, else false
*/
bool Knight::checkMove(const int dest[SIZE_OF_POS])
{
	//check iF the distance from the destination indexes is valid
	if ((abs(this->getPosition()[X_INDEX] - dest[X_INDEX]) == VALID_SECOND_DISTANCE && abs(this->getPosition()[Y_INDEX] - dest[Y_INDEX]) == VALID_FIRST_DISTANCE) ||
		(abs(this->getPosition()[X_INDEX] - dest[X_INDEX]) == VALID_FIRST_DISTANCE) && abs(this->getPosition()[Y_INDEX] - dest[Y_INDEX]) == VALID_SECOND_DISTANCE)
	{
		return true;
	}
	return false;
}


/*
Checks if Kngiht object is bloked, But beacuse knight never bloked in a chess game, it will return false automatically
*/
bool Knight::isBlocked(int* dest, Piece* board[][CHESS_BOARD_LEN])
{
	return false;
}
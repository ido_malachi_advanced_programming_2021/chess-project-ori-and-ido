#pragma once
#include "Menu.h"

#define MESSAGE_LEN 4

class FrontHandler
{
private:
	Menu* _IdoAndOriMenu;

public:  
	FrontHandler(Menu *gameMenu);
	~FrontHandler();
	char* getMessage();
	void sendMessage(const char* actionApproval);
	static void sendStartMessage();
	static void splitSrcAndDest(std::string msg, int src[SIZE_OF_POS], int dest[SIZE_OF_POS]);

};


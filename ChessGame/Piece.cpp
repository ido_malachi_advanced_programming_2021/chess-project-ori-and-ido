#include "Piece.h"

/*
The function Initializes a new Piece object
Input: The color of the Piece, the position of the Piece
Output: None
*/
Piece::Piece(const int color, const int position[SIZE_OF_POS])
{
	this->_color = color;
	this->_position[X_INDEX] = position[X_INDEX];
	this->_position[Y_INDEX] = position[Y_INDEX];
}
/*
The function returns the color of the Piece object
Input: None
Output: The Piece's color (int)
*/
int Piece::getColor() const
{
	return this->_color;
}

/*
The function returns the position of the Piece object
Input: None
Output: The Piece's position
*/
int* Piece::getPosition()
{
	return this->_position;
}

/*
The function returns the type of the Piece object
Input: None
Output: The Piece's type
*/
Type Piece::getType() const
{
	return this->_type;
}


/*
The function updates the field _position
Input: position to update the field _position to
Output: None
*/
void Piece::setPosition(const int position[SIZE_OF_POS])
{
	this->_position[X_INDEX] = position[X_INDEX];
	this->_position[Y_INDEX] = position[Y_INDEX];
}

/*
The function updates the field _type
Input: wanted type to change the field _type to
Output: None
*/
void Piece::setType(const Type type)
{
	this->_type = type;
}


/*
default function for isThretened, tho will be used only for king
*/
bool Piece::isThreatened(Piece* board[][CHESS_BOARD_LEN])
{
	return false;
}

#include "FrontHandler.h"

/*
The function initializes a new FrontHandler object
*/
FrontHandler::FrontHandler(Menu* gameMenu) :
	_IdoAndOriMenu(gameMenu)
{}

/*
The funtion deletes a FrontHandler object
*/
FrontHandler::~FrontHandler()
{
	delete this->_IdoAndOriMenu;
}

/*
The funtion sends the stsart message to the front so it will organize the board
*/
void FrontHandler::sendStartMessage()
{
	std::string startMessage = "RNBKQBNRPPPPPPPP################################pppppppprnbkqbnr0";
	std::cout << "Sent 66 bytes; message:" << "\"" << startMessage << "\"" << std::endl;
	//send to front instead of printing
}


/*
The function gets a message from the front (the squares) the user touched and returns it as a char pointer
*/
char* FrontHandler::getMessage()
{
	char* msg = new char[MESSAGE_LEN];
	std::string checkInput = "";
	std::cout << "Enter coordinates of source and destenation for a piece you want to move (example e4e5)  e4-src e5-dst: ";
	std::getline(std::cin, checkInput);
	while (checkInput.length() != MESSAGE_LEN)
	{
		std::cout << "Enter coordinates of source and destenation **length has to be 4** (example e4e5)  e4-src e5-dst: ";
		std::getline(std::cin, checkInput);
		std::cin.ignore();
	}
	for (int i = 0; i < MESSAGE_LEN; i++)
	{
		msg[i] = checkInput[i];
	}
	return msg;
	//get message from the front instead of getting it from the user 
}


/*
The function gets a message and splits it to dource and destenation
Input: message to split, source, dest
Output: None
*/
void FrontHandler::splitSrcAndDest(std::string msg, int src[SIZE_OF_POS], int dest[SIZE_OF_POS])
{
	src[X_INDEX] = msg[1] - ASCII_VALUE_OF_0 - 1;
	src[X_INDEX] = 7 - src[X_INDEX];
	dest[X_INDEX] = msg[3] - ASCII_VALUE_OF_0 -1;
	dest[X_INDEX] = 7 - dest[X_INDEX];

	src[Y_INDEX] = msg[0] - ASCII_VALUE_OF_A - 1;
	dest[Y_INDEX] = msg[2] - ASCII_VALUE_OF_A - 1;

}


/*
The function sends a string to the front that says weather the wanted action is valid or not
*/
void FrontHandler::sendMessage(const char* actionApproval)
{
	switch ((int)actionApproval[0] - ASCII_VALUE_OF_0)
	{
	case 0:
		std::cout << "Valid move" << std::endl;
		break;

	case 1:
		std::cout << "Valid move - your rival's king is thearetened YOU WON!!!" << std::endl;
		break;

	case 2:
		std::cout << "Invalid move - there isn't a soldier of the current player in the source square" << std::endl;
		break;

	case 3:
		std::cout << "Invalid move - there is a soldier of the current player in the destenation square" << std::endl;
		break;

	case 4:
		std::cout << "Invalid move - there isn't a soldier of the current player in the source square" << std::endl;
		break;

	case 5:
		std::cout << "Invalid source or destenation index" << std::endl;
		break;

	case 6:
		std::cout << "Invalid move - the soldier can't perform this move" << std::endl;
		break;

	case 7:
		std::cout << "Invalid move - destenation and source square are equale" << std::endl;
		break;

	case 8:
		std::cout << "Chessmate!!!" << std::endl;
		break;

	default:
		break;
	}
	//send message to the front instead of printing it to the user
}
